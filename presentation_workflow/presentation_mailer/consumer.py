import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approved")
channel.queue_declare(queue="presentation_rejected")


def process_approval(ch, method, properties, body):
    approve_body = json.loads(body)
    name = approve_body["presenter_name"]
    # email = approve_body["presenter_email"]
    title = approve_body["title"]
    subject = f"Your presentation {title} has been approved!"
    message = f"Hi {name}! Your presentation {title} has been approved!"
    send_mail(
        subject,
        message,
        "admin@conference.go",
        [approve_body["presenter_email"]],
        fail_silently=False,
    )


channel.basic_consume(
    queue="presentation_approved",
    on_message_callback=process_approval,
    auto_ack=True,
)


def process_rejection(ch, method, properties, body):
    reject_body = json.loads(body)
    name = reject_body["presenter_name"]
    # email = reject_body["presenter_email"]
    title = reject_body["title"]
    subject = f"Your presentation {title} has not been approved!"
    message = f"Hi {name}! Your presentation {title} has not been approved!"

    send_mail(
        subject,
        message,
        "admin@conference.go",
        [reject_body["presenter_email"]],
        fail_silently=False,
    )


channel.basic_consume(
    queue="presentation_rejected",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
